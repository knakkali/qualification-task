void vec_maxDVsPSnum(){

  TCanvas* c1 = new TCanvas();

  //First graph
  //1st graph

  TGraph* gr1 = new TGraph("log_vecmaxD.txt");
  gr1->SetTitle("Maximum KS distance vs Pixel subset number");
  gr1->SetMarkerStyle(8);
  gr1->GetXaxis()->SetTitle("Pixel subset number");
  gr1->GetYaxis()->SetTitle("Maximum KS distance");
  //gr1->GetYaxis()->SetRangeUser(0.65,0.70);
  gr1->GetXaxis()->CenterTitle(true);
  gr1->GetYaxis()->CenterTitle(true);
  gr1->SetMarkerColor(4);
  gr1->SetStats(1);
  gr1->Draw("AP");
  
}
