void Tree_BrlEC(){

  /*  TFile* f219 = TFile::Open("/Users/nakkalil/cernbox/athena_clones/21.9_MyAthenaWorkingFolder/run/PixelRDOAnalysis.root");
  if(f219 == 0) {
    std::cout << " Error! Cannot open the root file " <<std::endl;
    }*/

  TFile* f22 = TFile::Open("/Users/nakkalil/cernbox/athena_clones/Athena08022022/run/1000Events_ITkPixelRDOAnalysis.root");
  if(f22 == 0) {
    std::cout << " Error! Cannot open the root file " <<std::endl;
  }

  // TH1D* h_brlcharge_R219 = new TH1D("h_brlcharge_R219"," ", 100, 0, 70000);
  // TH1D* h_eccharge_R219 = new TH1D("h_eccharge_R219"," ", 100, 0, 70000);
  // TH1D* h_charge_R22 = new TH1D("h_charge_R22"," ", 100, 0, 70000);
  
  // TTree* tree_219 = (TTree*)f219->Get("PixelRDOAnalysis/PixelRDOAna");
  TTree* tree_22 = (TTree*)f22->Get("ITkPixelRDOAnalysis/ITkPixelRDOAnalysis");

  // tree_219->Project("h_brlcharge_R219","charge");
  // tree_219->Project("h_brlcharge_R219","charge");
  
  TCanvas* c = new TCanvas();
  c->SetLogy();
  //  h_charge_R219->SetLineColor(kBlue);
  // h_charge_R22->SetLineColor(kRed);
  // h_charge_R219->GetXaxis()->SetTitle("Charge");
  // h_charge_R219->Draw();
  // h_charge_R22->Draw("sames");
  tree_22->SetLineColor(kBlue);
  tree_22->Draw("charge","barrelEndcap_sdo==0");
  TH1D* htemp = (TH1D*)gPad->GetPrimitive("htemp");
  htemp->SetTitle("Barrel-Endcap charge (SDO) distribution in R22");
  gPad->Update();
  tree_22->SetLineColor(kRed);
  tree_22->Draw("charge","abs(barrelEndcap_sdo)==2", "sames");
  //  tree_219->Draw("charge","barrelEndcap_sdo==0 && layerDisk_sdo==0");

  TLegend *leg1 = new TLegend(0.55,0.65,0.76,0.82);
  leg1->AddEntry(tree_22,"Barrel charge(SDO) "," ");
  leg1->SetBorderSize(0);
  leg1->Draw();

  TLegend *leg2 = new TLegend(0.55,0.65,0.76,0.82);
  //leg1->AddEntry(tree_219,"Barrel charge(SDO) ","l");
  leg2->AddEntry(tree_22,"Endcap charge(SDO)"," ");
  leg2->SetBorderSize(0);
  leg2->Draw();
}
  
