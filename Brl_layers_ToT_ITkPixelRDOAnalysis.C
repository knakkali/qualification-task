void Brl_layers_ToT_ITkPixelRDOAnalysis() {
  string filename_f1 = "/Users/nakkalil/cernbox/athena_clones/Athena08022022/run/ITkPixelRDOAnalysis.root";
  // string filename_f1 = "/Users/nakkalil/cernbox/athena_clones/21.9_MyAthenaWorkingFolder/run/PixelRDOAnalysis.root";
  
  TFile* f1 = TFile::Open(filename_f1.c_str());

  string hist1 = "h_brlflatToT_perLayer0"; 
  string hist2 = "h_brlflatToT_perLayer1";
  string hist3 = "h_brlflatToT_perLayer2";
  string hist4 = "h_brlflatToT_perLayer3";
  string hist5 = "h_brlflatToT_perLayer4";

  TH1F* h1 = static_cast<TH1F*>(f1->Get(hist1.c_str()));
  TH1F* h2 = static_cast<TH1F*>(f1->Get(hist2.c_str()));
  TH1F* h3 = static_cast<TH1F*>(f1->Get(hist3.c_str()));
  TH1F* h4 = static_cast<TH1F*>(f1->Get(hist4.c_str()));
  TH1F* h5 = static_cast<TH1F*>(f1->Get(hist5.c_str()));

  TCanvas* c = new TCanvas();
  c->Divide(3,2);

  c->cd(1);
  h1->Draw();

  c->cd(2);
  h2->Draw();

  c->cd(3);
  h3->Draw();

  c->cd(4);
  h4->Draw();

  c->cd(5);
  h5->Draw();
}
